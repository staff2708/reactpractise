module.exports = {
    env: {
        es6: true,
        browser: true,
        node: true,
    },
    extends: ['airbnb', 'plugin:jest/recommended', 'jest-enzyme'],
    plugins: ['babel', 'jsx-a11y', 'react', 'prettier'],
    parser: 'babel-eslint',
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
        },
    },
    rules: {
        'no-restricted-globals': 'warn',
        'linebreak-style': 'off', // Don't play nicely with Windows.
        'import/named': 'off',
        'import/no-named-as-default': 'off',
        'import/prefer-default-export': 'off',
        indent: ['error', 4],
        'arrow-parens': 'off', // Incompatible with prettier
        'object-curly-newline': 'off', // Incompatible with prettier
        'no-mixed-operators': 'off', // Incompatible with prettier
        'arrow-body-style': 'off', // Not our taste?
        'function-paren-newline': 'off', // Incompatible with prettier
        'no-plusplus': 'off',
        'space-before-function-paren': 0, // Incompatible with prettier
        'jsx-a11y/no-static-element-interactions': 'off',
        'jsx-a11y/click-events-have-key-events': 'off',
        'camelcase': 'warn',

        'max-len': ['error', 120, 2, { ignoreUrls: true }], // airbnb is allowing some edge cases
        'no-console': 'warn', // airbnb is using warn
        'no-alert': 'error', // airbnb is using warn

        'no-param-reassign': 'off', // Not our taste?
        radix: 'off', // parseInt, parseFloat radix turned off. Not my taste.

        'react/prop-types': 'off',
        'react/button-has-type':'off',
        'react/require-default-props': 'off', // airbnb use error
        'react/forbid-prop-types': 'off', // airbnb use error
        'react/jsx-filename-extension': ['error', { extensions: ['.js', '.jsx'] }], // airbnb is using .jsx

        'prefer-destructuring': 'off',

        'react/no-find-dom-node': 'off', // I don't know
        'react/no-did-mount-set-state': 'off',
        'react/no-unused-prop-types': 'off', // Is still buggy
        'react/jsx-one-expression-per-line': 'off',
        'react/jsx-indent': ['error', 4],
        'react/jsx-indent-props': 'off',

        'jsx-a11y/anchor-is-valid': ['error', { components: ['Link'], specialLink: ['to'] }],
        'jsx-a11y/label-has-for': [
            2,
            {
                required: {
                    every: ['id'],
                },
            },
        ], // for nested label htmlFor error

        'prettier/prettier': ['warn'],
    },
};
