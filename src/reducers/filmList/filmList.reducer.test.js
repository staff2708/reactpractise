import filmListReducer, { getFoundedFilmsByID, getSortBy, getSortedFoundedFilms, parseByID } from './filmList.reducer';
import { filmListActionsTypes } from '../../actions/filmList/filmList.actions';

function getInitData() {
    const filmListReducerInitialState = {
        foundedFilms: {
            byID: {},
            allID: [],
        },
        sortBy: 'release_date',
    };

    const mockData = [
        {
            id: 1,
            name: 'Star Lord',
            release_date: 1234,
            vote_average: 4,
        },
        {
            id: 2,
            name: 'Darth Vader',
            release_date: 321,
            vote_average: 5,
        },
    ];

    const reducerMockStateWithReleaseSort = {
        foundedFilms: {
            byID: {
                1: mockData[0],
                2: mockData[1],
            },
            allID: [1, 2],
        },
        sortBy: 'release_date',
    };

    const reducerMockStateWithRatingSort = {
        foundedFilms: {
            byID: {
                1: mockData[0],
                2: mockData[1],
            },
            allID: [1, 2],
        },
        sortBy: 'rating',
    };

    return {
        reducerInitData: filmListReducerInitialState,
        mockDataForParse: mockData,
        fullMockData: {
            data: mockData,
        },
        reducerMockStateWithReleaseSort,
        reducerMockStateWithRatingSort,
    };
}

describe('film list reducer', () => {
    it('should create reducer', () => {
        const { reducerInitData } = getInitData();
        expect(filmListReducer(undefined, 'Init')).toEqual(reducerInitData);
    });

    it('should parse data correct', () => {
        const { mockDataForParse } = getInitData();
        const expectedData = {
            1: mockDataForParse[0],
            2: mockDataForParse[1],
        };
        expect(parseByID(mockDataForParse)).toEqual(expectedData);
    });

    it('should return sort by from state', () => {
        const { reducerInitData } = getInitData();
        const expectedResult = 'release_date';
        expect(getSortBy(reducerInitData)).toBe(expectedResult);
    });

    it('should return founded films from state', () => {
        const { reducerInitData } = getInitData();
        const expectedResult = {
            byID: {},
            allID: [],
        };
        expect(getFoundedFilmsByID(reducerInitData)).toEqual(expectedResult);
    });
    /* @ TODO add spinner and use this action for flag change */
    it('should handle action REQUEST_DATA', () => {
        const { reducerInitData } = getInitData();
        const mockAction = {
            type: filmListActionsTypes.REQUEST_DATA,
        };
        expect(filmListReducer(undefined, mockAction)).toEqual(reducerInitData);
    });

    it('should handle action REQUEST_SUCCESS', () => {
        const { mockDataForParse, fullMockData } = getInitData();
        const mockAction = {
            type: filmListActionsTypes.REQUEST_SUCCESS,
            res: fullMockData,
        };
        const expectedResult = {
            foundedFilms: {
                byID: {
                    1: mockDataForParse[0],
                    2: mockDataForParse[1],
                },
                allID: [1, 2],
            },
            sortBy: 'release_date',
        };
        expect(filmListReducer(undefined, mockAction)).toEqual(expectedResult);
    });

    it('should handle action CHANGE_SORT_TYPE', () => {
        const mockAction = {
            type: filmListActionsTypes.CHANGE_SORT_TYPE,
            sortBy: 'rating',
        };
        const expectedResult = {
            foundedFilms: {
                byID: {},
                allID: [],
            },
            sortBy: 'rating',
        };
        expect(filmListReducer(undefined, mockAction)).toEqual(expectedResult);
    });

    it('should select correct data', () => {
        const { reducerMockStateWithReleaseSort, reducerMockStateWithRatingSort } = getInitData();
        const expectedReleaseSort = {
            byID: {
                1: {
                    id: 1,
                    name: 'Star Lord',
                    release_date: 1234,
                    vote_average: 4,
                },
                2: {
                    id: 2,
                    name: 'Darth Vader',
                    release_date: 321,
                    vote_average: 5,
                },
            },
            allID: [2, 1],
        };

        const expectedRatingSort = {
            byID: {
                1: {
                    id: 1,
                    name: 'Star Lord',
                    release_date: 1234,
                    vote_average: 4,
                },
                2: {
                    id: 2,
                    name: 'Darth Vader',
                    release_date: 321,
                    vote_average: 5,
                },
            },
            allID: [1, 2],
        };
        expect(getSortedFoundedFilms(reducerMockStateWithReleaseSort)).toEqual(expectedReleaseSort);
        expect(getSortedFoundedFilms(reducerMockStateWithRatingSort)).toEqual(expectedRatingSort);
    });
});
