import { createSelector } from 'reselect';
import { filmListActionsTypes } from '../../actions/filmList/filmList.actions';

const filmListReducerInitialState = {
    foundedFilms: {
        byID: {},
        allID: [],
    },
    sortBy: 'release_date',
};

export const parseByID = data => {
    return data.reduce((acc, item) => {
        acc[item.id] = item;
        return acc;
    }, {});
};

function filmListReducer(state = filmListReducerInitialState, action) {
    switch (action.type) {
        case filmListActionsTypes.REQUEST_DATA:
            return state;
        case filmListActionsTypes.REQUEST_SUCCESS:
            return {
                ...state,
                foundedFilms: {
                    byID: parseByID(action.res.data),
                    allID: action.res.data.map(item => item.id),
                },
            };
        case filmListActionsTypes.CHANGE_SORT_TYPE:
            return {
                ...state,
                sortBy: action.sortBy,
            };
        default:
            return state;
    }
}

export const getSortBy = state => state.sortBy;
export const getFoundedFilmsByID = state => state.foundedFilms;

export const getSortedFoundedFilms = createSelector(
    [getFoundedFilmsByID, getSortBy],
    (foundedFilms, sortBy) => {
        if (sortBy === 'release_date') {
            return {
                ...foundedFilms,
                allID: foundedFilms.allID.sort(
                    (a, b) => new Date(foundedFilms.byID[a].release_date) - new Date(foundedFilms.byID[b].release_date),
                ),
            };
        }
        return {
            ...foundedFilms,
            allID: foundedFilms.allID.sort(
                (a, b) => foundedFilms.byID[a].vote_average - foundedFilms.byID[b].vote_average,
            ),
        };
    },
);

export default filmListReducer;
