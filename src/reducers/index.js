import { combineReducers } from 'redux';
import filmListReducer from './filmList/filmList.reducer';
import headerReducer from './header/header.reducer';

const rootReducer = combineReducers({ filmListReducer, headerReducer });

export default rootReducer;
