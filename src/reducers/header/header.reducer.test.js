import headerReducer from './header.reducer';
import { ChangeFindFilter, SelectFilm } from '../../actions/header/header.actions';

const headerReducerInitialState = {
    selectedFilm: {},
    findBy: 'title',
};

describe('header reducer', () => {
    it('should return initial state on undefined state value', () => {
        expect(headerReducer(undefined, 'Init')).toEqual(headerReducerInitialState);
    });

    it('should return selected film', () => {
        const mockFilm = {
            title: 'Star Wars Episode 1',
        };
        const expectedState = {
            selectedFilm: {
                title: 'Star Wars Episode 1',
            },
            findBy: 'title',
        };
        expect(headerReducer(undefined, new SelectFilm(mockFilm))).toEqual(expectedState);
    });

    it('should return selected findBy', () => {
        const mockFindBy = 'genre';
        const expectedState = {
            selectedFilm: {},
            findBy: 'genre',
        };
        expect(headerReducer(undefined, new ChangeFindFilter(mockFindBy))).toEqual(expectedState);
    });
});
