import { headerActionsTypes } from '../../actions/header/header.actions';

const headerReducerInitialState = {
    selectedFilm: {},
    findBy: 'title',
};

function headerReducer(state = headerReducerInitialState, action) {
    switch (action.type) {
        case headerActionsTypes.SELECT_FILM:
            return { ...state, selectedFilm: action.film };
        case headerActionsTypes.CHANGE_FILTER:
            return { ...state, findBy: action.findBy };
        default:
            return state;
    }
}

export default headerReducer;
