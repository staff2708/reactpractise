import { applyMiddleware, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import hardSet from 'redux-persist/lib/stateReconciler/hardSet';

import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';

import rootReducer from './reducers';

const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: hardSet,
};

const persist = persistReducer(persistConfig, rootReducer);

export default function configureStore() {
    const store = createStore(persist, applyMiddleware(thunkMiddleware, logger));
    const persistor = persistStore(store);
    return { store, persistor };
}
