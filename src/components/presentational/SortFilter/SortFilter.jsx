import React from 'react';
import './SortFilter.style.css';
import Button from '../Button/Button';
import classNames from 'classnames';

class SortFilter extends React.PureComponent {
    handleOnClick = event => {
        const { changeSortType } = this.props;
        changeSortType(event.target.id);
    };

    render() {
        const { searchResult, sortBy } = this.props;
        const sortFilterReleaseDateBtn = classNames(
            'sort-filter__button',
            'sort-filter__button--release',
            { 'sort-filter__button--active': sortBy === 'release_date' },
        );
        const sortFilterRatingBtn = classNames(
            'sort-filter__button',
            'sort-filter__button--rating',
            { 'sort-filter__button--active': sortBy === 'rating' },
        );
        return (
            <div className="sort-filter">
                <div className="sort-filter__wrapper">
                    <div className="sort-filter__search-results"> {searchResult} </div>
                    <div className="sort-filter__container">
                        <span> Sort by </span>
                        <Button
                            text="release date"
                            styleName={sortFilterReleaseDateBtn}
                            id="release_date"
                            onClick={this.handleOnClick}
                        />
                        <Button
                            text="rating"
                            styleName={sortFilterRatingBtn}
                            id="rating"
                            onClick={this.handleOnClick}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default SortFilter;
