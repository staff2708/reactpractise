import React from 'react';
import { shallow, mount } from 'enzyme';
import SortFilter from './SortFilter';

describe('SearchFilter component', () => {
    it('should match snapshot', () => {
        const sut = shallow(<SortFilter searchResult="founded 7 films" />);
        expect(sut).toMatchSnapshot();
    });

    it('should handle call action on filter change', () => {
        const changeSortType = jest.fn();
        const sut = mount(
            <SortFilter searchResult="founded 7 films" changeSortType={changeSortType} />,
        );
        sut.find('.sort-filter__button--release').simulate('click');
        expect(changeSortType).toHaveBeenCalled();
        expect(changeSortType).toHaveBeenCalledWith('release_date');
        sut.find('.sort-filter__button--rating').simulate('click');
        expect(changeSortType).toHaveBeenCalledTimes(2);
        expect(changeSortType).toHaveBeenCalledWith('rating');
    });

    it('should match snapshot with active release date filter', () => {
        const sut = shallow(<SortFilter searchResult="founded 7 films" sortBy="release_date" />);
        expect(sut).toMatchSnapshot();
    });

    it('should match snapshot with active rating filter', () => {
        const sut = shallow(<SortFilter searchResult="founded 7 films" sortBy="rating" />);
        expect(sut).toMatchSnapshot();
    });
});
