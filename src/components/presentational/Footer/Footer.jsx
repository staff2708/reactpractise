import React from 'react';
import './Footer.style.css';

const Footer = () => {
    return (
        <div className="footer">
            <div className="footer__wrapper">
                <h3 className="footer__title">Hello World</h3>
            </div>
        </div>
    );
};

export default Footer;
