import React from 'react';
import { shallow } from 'enzyme';
import SearchFilter from './SearchFilter';

describe('SearchFilter component', () => {
    it('should match snapshot', () => {
        const sut = shallow(<SearchFilter />);
        expect(sut).toMatchSnapshot();
    });

    it('should match snapshot when filter by title', () => {
        const sut = shallow(<SearchFilter findBy="title" />);
        expect(sut).toMatchSnapshot();
    });

    it('should match snapshot when filter by genres', () => {
        const sut = shallow(<SearchFilter findBy="genres" />);
        expect(sut).toMatchSnapshot();
    });
});
