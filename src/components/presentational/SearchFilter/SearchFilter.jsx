import React from 'react';
import Button from '../Button/Button';
import './SearchFilter.style.css';
import classNames from 'classnames';
/* global event */

class SearchFilter extends React.PureComponent {
    handleOnClick = event => {
        const { onChangeSearchBy } = this.props;
        onChangeSearchBy(event.target.id);
    };

    render() {
        const { findBy } = this.props;
        const searchFilterTitleBtn = classNames(
            'search-filter__button',
            'search-filter__button--title',
            { 'search-filter__button--active': findBy === 'title' },
        );
        const searchFilterGenreBtn = classNames(
            'search-filter__button',
            'search-filter__button--genre',
            { 'search-filter__button--active': findBy === 'genres' },
        );
        return (
            <div className="search-filter__wrapper">
                <h3 className="search-filter__text">search by</h3>
                <Button
                    text="Title"
                    styleName={searchFilterTitleBtn}
                    id="title"
                    onClick={this.handleOnClick}
                />
                <Button
                    text="Genre"
                    styleName={searchFilterGenreBtn}
                    id="genres"
                    onClick={this.handleOnClick}
                />
            </div>
        );
    }
}

export default SearchFilter;
