import React from 'react';
import './Search.style.css';
import Button from '../Button/Button';
import SearchFilter from '../SearchFilter/SearchFilter';
import Input from '../Input/Input';

const Search = ({ onSearch, onChangeSearchBy, findBy }) => {
    const InputRef = React.createRef();
    const onClick = () => {
        onSearch(InputRef.current.value);
    };
    return (
        <div>
            <h2 className="search__title"> Find Your Movie </h2>
            <Input styleClass="search__input" customRef={InputRef} />
            <div className="search__container">
                <SearchFilter onChangeSearchBy={onChangeSearchBy} findBy={findBy} />
                <Button text="Search" styleName="search__button" onClick={onClick} />
            </div>
        </div>
    );
};

export default Search;
