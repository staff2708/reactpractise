import React from 'react';
import { shallow } from 'enzyme';
import Search from './Search';

describe('Search component', () => {
    it('should match snapshot', () => {
        const sut = shallow(<Search />);
        expect(sut).toMatchSnapshot();
    });
});
