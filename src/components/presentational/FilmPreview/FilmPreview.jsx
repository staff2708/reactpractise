import React from 'react';
import './FilmPreview.style.css';

const FilmPreview = ({
    filmImageUrl,
    filmName,
    filmReleaseDate,
    filmGenres,
    filmID,
    handleFilmSelect,
}) => {
    const handleFilmClick = () => {
        handleFilmSelect(filmID);
    };

    const getYear = date => {
        return new Date(date).getFullYear();
    };

    return (
        <div className="film-preview__wrapper" onClick={handleFilmClick}>
            <div className="film-preview__image-wrapper">
                <img src={filmImageUrl} alt="" className="film-preview__image" />
            </div>
            <div className="film-preview__name-date-container">
                <h4 className="film-preview__name">{filmName}</h4>
                <span className="film-preview__date">{getYear(filmReleaseDate)}</span>
            </div>
            <div className="film-preview__genres">
                {filmGenres.map(genre => (
                    <span className="film-preview__genre" key={genre}>
                        {genre}
                    </span>
                ))}
            </div>
        </div>
    );
};

export default FilmPreview;
