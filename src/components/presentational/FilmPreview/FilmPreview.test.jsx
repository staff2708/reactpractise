import React from 'react';
import { shallow } from 'enzyme';
import FilmPreview from './FilmPreview';

describe('FilmPreview component', () => {
    it('should match snapshot', () => {
        const props = { filmImageUrl: '', filmName: '', filmReleaseDate: '', filmGenres: [] };
        const sut = shallow(<FilmPreview {...props} />);
        expect(sut).toMatchSnapshot();
    });
});
