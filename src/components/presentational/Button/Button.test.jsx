import React from 'react';
import { shallow } from 'enzyme';
import Button from './Button';

describe('Button component', () => {
    it('should match snapshot', () => {
        const sut = shallow(<Button />);
        expect(sut).toMatchSnapshot();
    });
    it('should render text', () => {
        const sut = shallow(<Button text="Click me" />);
        expect(sut.find('button').text()).toBe('Click me');
    });
});
