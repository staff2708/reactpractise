import React from 'react';
import './Button.style.css';

const Button = ({ text, styleName, id, onClick }) => {
    return (
        <button className={styleName} id={id} onClick={onClick}>
            {text}
        </button>
    );
};

export default Button;
