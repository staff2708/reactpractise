import React from 'react';
import './Input.style.css';
import classNames from 'classnames';
class Input extends React.PureComponent {
    render() {
        const { customRef, styleClass } = this.props;
        const wrapperClassName = classNames(styleClass, 'wrapper');
        return (
            <div className={wrapperClassName}>
                <input type="text" ref={customRef} className="custom__input" />
                <span className="underline" />
            </div>
        );
    }
}

export default Input;
