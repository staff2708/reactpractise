import React from 'react';
import { shallow } from 'enzyme';
import Input from './Input';

describe('InputComponent', () => {
    it('should match snapshot', () => {
        const sut = shallow(<Input />);
        expect(sut).toMatchSnapshot();
    });
});
