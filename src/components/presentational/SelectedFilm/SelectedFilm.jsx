import React from 'react';
import './SelectedFilm.style.css';

const SelectedFilm = ({ film }) => {
    const year = new Date(film.release_date).getFullYear();
    return (
        <div className="selected-film__wrapper">
            <div className="selected-film__image-wrapper">
                <img className="selected-film_poster" src={film.poster_path} alt="" />
            </div>
            <div className="selected-film__content-wrapper">
                <div className="container">
                    <h3 className="selected-film__name">{film.title}</h3>
                    <span className="selected-film__vote-average">{film.vote_average}</span>
                </div>
                <div className="selected-film__tagline">{film.tagline}</div>
                <span className="selected-film__release-date">{year}</span>
                <span className="selected-film__runtime">{film.runtime} min</span>
                <div className="selected-film__overview">{film.overview}</div>
            </div>
        </div>
    );
};

export default SelectedFilm;
