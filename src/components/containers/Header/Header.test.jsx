import React from 'react';
import { shallow } from 'enzyme';
import { Header } from './Header';

describe('Header component', () => {
    it('should match snapshot without selected film', () => {
        const sut = shallow(<Header />);
        expect(sut).toMatchSnapshot();
    });
});
