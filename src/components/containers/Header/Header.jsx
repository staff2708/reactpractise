import React from 'react';
import './Header.style.css';

export const Header = ({ children }) => <div>{children}</div>;

export default Header;
