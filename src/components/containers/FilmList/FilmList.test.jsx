import React from 'react';
import { shallow } from 'enzyme';
import { FilmList } from './FilmList';

describe('FilmList component', () => {
    it('should match snapshot', () => {
        const sut = shallow(<FilmList />);
        expect(sut).toMatchSnapshot();
    });
});
