import React from 'react';
import './FilmList.style.css';

export const FilmList = ({ children }) => <div>{children}</div>;

export default FilmList;
