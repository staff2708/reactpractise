import { SelectFilm } from '../header/header.actions';

export const filmListActionsTypes = {
    REQUEST_DATA: 'Fetch data from api',
    REQUEST_SUCCESS: 'Data successfully fetched',
    REQUEST_ERROR: 'Error during fetch data',
    CHANGE_SORT_TYPE: 'Change sort by',
};

/* global api */

export function RequestData(findBy, filterType) {
    return {
        type: 'Fetch data from api',
        findBy,
        filterType,
    };
}

export function RequestSuccess(res) {
    return {
        type: 'Data successfully fetched',
        res,
    };
}

export function RequestError(err) {
    return {
        type: 'Error during fetch data',
        err,
    };
}

export function ChangeSortType(sortBy) {
    return {
        type: 'Change sort by',
        sortBy,
    };
}

export function fetchData(findBy, filterBy) {
    return dispatch => {
        // dispatch(new RequestData());
        const urlToFetch = new URL('https://reactjs-cdp.herokuapp.com/movies');
        if (findBy) {
            urlToFetch.searchParams.append('search', findBy);
        }
        if (filterBy) {
            urlToFetch.searchParams.append('searchBy', filterBy);
        }
        return fetch(urlToFetch)
            .then(res => {
                if (res.status === 200 && res.json) {
                    return res.json();
                }
                if (res.status === 400) {
                    throw Error('Bad Request');
                }

                if (res.status === 404) {
                    throw Error('Not Found');
                }

                throw Error('Unknown Error');
            })
            .then(data => dispatch(RequestSuccess(data)))
            .catch(err => dispatch(RequestError(err.message)));
    };
}

export function fetchFilm(filmId) {
    return dispatch => {
        const urlToFetch = new URL(`https://reactjs-cdp.herokuapp.com/movies/${filmId}`);

        return fetch(urlToFetch)
            .then(res => {
                if (res.status === 200 && res.json) {
                    return res.json();
                }
                if (res.status === 400) {
                    throw Error('Bad Request');
                }

                if (res.status === 404) {
                    throw Error('Not Found');
                }

                throw Error('Unknown Error');
            })
            .then(data => dispatch(SelectFilm(data)))
            .catch(err => dispatch(RequestError(err.message)));
    };
}
