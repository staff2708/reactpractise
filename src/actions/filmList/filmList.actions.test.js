import '@babel/polyfill';
import thunkMiddleware from 'redux-thunk';
import configureStore from 'redux-mock-store'
import fetchMock from 'fetch-mock';
import { ChangeSortType, fetchData, RequestData, RequestError, RequestSuccess } from './filmList.actions';

const middlewares = [thunkMiddleware];
const mockStore = configureStore(middlewares);

describe('Film List Actions', () => {
    it('should create RequestData action', () => {
        const expectedAction = {
            type: 'Fetch data from api',
            findBy: 'Star Wars',
            filterType: 'title',
        };

        expect(expectedAction)
            .toEqual(RequestData('Star Wars', 'title'));
    });

    it('should create RequestSuccess action', () => {
        const expectedAction = {
            type: 'Data successfully fetched',
            res: { data: [] },
        };

        expect(expectedAction)
            .toEqual(RequestSuccess({ data: [] }));
    });

    it('should create RequestError action', () => {
        const expectedAction = {
            type: 'Error during fetch data',
            err: 'You have no permission',
        };

        expect(expectedAction)
            .toEqual(RequestError('You have no permission'));
    });

    it('should create change sort type action', () => {
        const expectedAction = {
            type: 'Change sort by',
            sortBy: 'release_date',
        };

        expect(expectedAction)
            .toEqual(ChangeSortType('release_date'));
    });

    it('should fetch data correct', async () => {
        fetchMock.getOnce(
            'https://reactjs-cdp.herokuapp.com/movies?search=Star+Wars&searchBy=genre',
            {
                headers: { 'content-type': 'application/json'},
                body: [],
            });

        const filmListReducerInitialState = {
            foundedFilms: {
                byID: {},
                allID: [],
            },
            sortBy: 'release_date',
        };

        const store = mockStore(filmListReducerInitialState);

        const expectedActions = [{ type: 'Data successfully fetched', res: [] }];
        await store.dispatch(fetchData('Star Wars', 'genre')).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });

        fetchMock.restore();
        fetchMock.getOnce('https://reactjs-cdp.herokuapp.com/movies?search=Star+Wars', {
            headers: { 'content-type': 'application/json' },
            body: [],
        });
        expectedActions.push({ type: 'Data successfully fetched', res: [] });
        await store.dispatch(fetchData('Star Wars')).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });

        fetchMock.restore();
        fetchMock.getOnce('https://reactjs-cdp.herokuapp.com/movies', {
            headers: { 'content-type': 'application/json' },
            body: [],
        });
        expectedActions.push({ type: 'Data successfully fetched', res: [] });
        await store.dispatch(fetchData()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });

        fetchMock.restore();
        fetchMock.getOnce('https://reactjs-cdp.herokuapp.com/movies?search=qwerty', 400);
        expectedActions.push({ type: 'Error during fetch data', err: 'Bad Request' });
        await store.dispatch(fetchData('qwerty')).then(() => {
            const actions = store.getActions();
            expect(actions).toEqual(expectedActions);
        });

        fetchMock.restore();
        fetchMock.getOnce('https://reactjs-cdp.herokuapp.com/movies?search=qwertys', 404);
        expectedActions.push({ type: 'Error during fetch data', err: 'Not Found' });
        await store.dispatch(fetchData('qwertys')).then(() => {
            const actions = store.getActions();
            expect(actions).toEqual(expectedActions);
        });

        fetchMock.restore();
        fetchMock.getOnce('https://reactjs-cdp.herokuapp.com/movies?search=qwertys', 500);
        expectedActions.push({ type: 'Error during fetch data', err: 'Unknown Error' });
        await store.dispatch(fetchData('qwertys')).then(() => {
            const actions = store.getActions();
            expect(actions).toEqual(expectedActions);
        });
    });
});
