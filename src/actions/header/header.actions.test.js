import { ChangeFindFilter, SelectFilm } from './header.actions';

describe('Header actions creator', () => {
    it('should create Select film object', () => {
        const expectedAction = {
            type: 'Select film',
            film: {
                title: 'Star Wars',
                id: '1',
            }
        };
        expect(expectedAction).toEqual(SelectFilm({ title: 'Star Wars', id: '1' }));
    });

    it('should create change find filter action', () => {
        const expectedAction = {
            type: 'Change filter from film finding',
            findBy: 'title',
        };
        expect(expectedAction).toEqual(ChangeFindFilter('title'));
    });
});
