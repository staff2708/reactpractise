export const headerActionsTypes = {
    SELECT_FILM: 'Select film',
    CHANGE_FILTER: 'Change filter from film finding',
};

export function SelectFilm(film) {
    return {
        type: 'Select film',
        film,
    };
}

export function ChangeFindFilter(findBy) {
    return {
        type: 'Change filter from film finding',
        findBy,
    };
}
