import '@babel/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';

import './style.css';
import Header from './components/containers/Header/Header';
import Footer from './components/presentational/Footer/Footer';
import configureStore from './configureStore';
import FilmSearchPage from './pages/FilmSerachPage/FilmSearchPage';
import FilmViewPage from './pages/FilmViewPage/FilmViewPage';
import FilmListPage from './pages/FilmListPage/FilmListPage';
import FilmList from './components/containers/FilmList/FilmList';

const { store, persistor } = configureStore();

const App = () => {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <Router>
                    <Switch>
                        <Route
                            path="/film/:id"
                            render={() => (
                                <Header>
                                    <FilmViewPage />
                                </Header>
                            )}
                        />
                        <Route
                            path="/"
                            render={() => (
                                <Header>
                                    <FilmSearchPage />
                                </Header>
                            )}
                        />
                    </Switch>
                    <Route
                        path="/:search?"
                        render={() => (
                            <FilmList>
                                <FilmListPage />
                            </FilmList>
                        )}
                    />
                    <Footer />
                </Router>
            </PersistGate>
        </Provider>
    );
};

ReactDOM.render(<App />, document.getElementById('app'));
