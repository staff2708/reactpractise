import React from 'react';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchData, fetchFilm } from '../../actions/filmList/filmList.actions';
import Search from '../../components/presentational/Search/Search';
import { ChangeFindFilter, SelectFilm } from '../../actions/header/header.actions';

export class FilmSearchPage extends React.PureComponent {
    onSearch = searchText => {
        const { findFilms, findBy, history } = this.props;
        findFilms(searchText, findBy);
        const url = `/search?searchBy=${searchText}&findBy=${findBy}`;
        history.push(url);
    };

    onChangeSearchBy = searchBy => {
        const { changeFindFilter } = this.props;
        changeFindFilter(searchBy);
    };

    render() {
        const { findBy } = this.props;
        return (
            <header className="header">
                <div className="header__wrapper">
                    <h3 className="header__title">Hello World</h3>
                    <Search
                        onSearch={this.onSearch}
                        onChangeSearchBy={this.onChangeSearchBy}
                        findBy={findBy}
                    />
                </div>
            </header>
        );
    }
}

const mapStateToProps = state => ({
    selectedFilm: state.headerReducer.selectedFilm,
    findBy: state.headerReducer.findBy,
});

const mapDispatchToProps = {
    fetchFilmById: fetchFilm,
    selectFilm: SelectFilm,
    changeFindFilter: ChangeFindFilter,
    findFilms: fetchData,
};

export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
)(FilmSearchPage);
