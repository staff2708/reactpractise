import React from 'react';
import SortFilter from '../../components/presentational/SortFilter/SortFilter';
import { getSortedFoundedFilms } from '../../reducers/filmList/filmList.reducer';
import { ChangeSortType, fetchData } from '../../actions/filmList/filmList.actions';
import { SelectFilm } from '../../actions/header/header.actions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';

class FilmNotFoundPage extends React.PureComponent {
    render() {
        const { sortBy, changeSortType } = this.props;
        return (
            <React.Fragment>
                <SortFilter
                    searchResult="Film not found"
                    sortBy={sortBy}
                    changeSortType={changeSortType}
                />
                <div className="no-films">
                    <h2 className="film-list__not-found">No Films Found</h2>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    data: getSortedFoundedFilms(state.filmListReducer),
    sortBy: state.filmListReducer.sortBy,
});

const mapDispatchToProps = {
    requestFilms: fetchData,
    selectFilm: SelectFilm,
    changeSortType: ChangeSortType,
};

export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
)(FilmNotFoundPage);
