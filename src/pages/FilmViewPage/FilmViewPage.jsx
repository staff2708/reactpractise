import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import React from 'react';
import { fetchData, fetchFilm } from '../../actions/filmList/filmList.actions';
import { ChangeFindFilter, SelectFilm } from '../../actions/header/header.actions';
import Button from '../../components/presentational/Button/Button';
import SelectedFilm from '../../components/presentational/SelectedFilm/SelectedFilm';

class FilmViewPage extends React.PureComponent {
    componentDidMount() {
        const { fetchFilmById, history } = this.props;
        if (history.location && history.location.pathname.includes('film')) {
            const id = history.location.pathname.split('/').splice(-1)[0];
            fetchFilmById(id);
        }
    }

    clearFilm = () => {
        const { selectFilm, history } = this.props;
        selectFilm({});
        history.push('/');
    };

    render() {
        const { selectedFilm } = this.props;
        return (
            <header className="header">
                <div className="header__wrapper">
                    <div className="container header__container">
                        <h3 className="header__title">Hello World</h3>
                        <Button
                            styleName="header__search-button"
                            text="search"
                            onClick={this.clearFilm}
                        />
                    </div>
                    <SelectedFilm film={selectedFilm} />
                </div>
            </header>
        );
    }
}

const mapStateToProps = state => ({
    selectedFilm: state.headerReducer.selectedFilm,
    findBy: state.headerReducer.findBy,
});

const mapDispatchToProps = {
    fetchFilmById: fetchFilm,
    selectFilm: SelectFilm,
    changeFindFilter: ChangeFindFilter,
    findFilms: fetchData,
};

export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
)(FilmViewPage);
