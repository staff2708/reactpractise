import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import SortFilter from '../../components/presentational/SortFilter/SortFilter';
import FilmPreview from '../../components/presentational/FilmPreview/FilmPreview';
import { getSortedFoundedFilms } from '../../reducers/filmList/filmList.reducer';
import { ChangeSortType, fetchData } from '../../actions/filmList/filmList.actions';
import { SelectFilm } from '../../actions/header/header.actions';
import FilmNotFoundPage from '../FilmNotFoundPage/FilmNotFoundPage';

class FilmListPage extends React.PureComponent {
    componentDidMount() {
        const { history, requestFilms } = this.props;
        if (history.location && history.location.pathname.includes('search')) {
            const urlParams = new URLSearchParams(window.location.search);
            requestFilms(urlParams.get('searchBy'), urlParams.get('findBy'));
        }
    }

    handleFilmSelect = filmID => {
        const { selectFilm, data, history } = this.props;
        selectFilm(data.byID[filmID]);
        history.push(`/film/${filmID}`);
    };

    render() {
        const { changeSortType, sortBy, data } = this.props;
        if (data.allID.length) {
            return (
                <React.Fragment>
                    <SortFilter
                        searchResult={`${data.allID.length} films found`}
                        changeSortType={changeSortType}
                        sortBy={sortBy}
                    />
                    <div className="film-list__wrapper">
                        {data.allID.map(filmID => {
                            const { poster_path, genres, release_date, title, id } = data.byID[
                                filmID
                            ];
                            return (
                                <FilmPreview
                                    filmImageUrl={poster_path}
                                    filmGenres={genres}
                                    filmReleaseDate={release_date}
                                    filmName={title}
                                    filmID={id}
                                    key={id}
                                    handleFilmSelect={this.handleFilmSelect}
                                />
                            );
                        })}
                    </div>
                </React.Fragment>
            );
        }
        return <FilmNotFoundPage />;
    }
}

const mapStateToProps = state => ({
    data: getSortedFoundedFilms(state.filmListReducer),
    sortBy: state.filmListReducer.sortBy,
});

const mapDispatchToProps = {
    requestFilms: fetchData,
    selectFilm: SelectFilm,
    changeSortType: ChangeSortType,
};

export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
)(FilmListPage);
