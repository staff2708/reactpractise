const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 8080;
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const db = require('./src/mock/db.json');
app.use(express.static('dist'));

app.get('*', (req,res) =>{
    res.sendFile(path.join(__dirname+'/dist/index.html'));
});

app.listen(port, () => {
    console.log(`node server listen to you on port ${port}`);
    if ( process.argv.includes('--test') ) {
        exec('nightwatch')
            .then(
                () => console.log('All tests is passed')
            )
            .catch((err)=> console.log('Error During tests', err))
            .then(() => process.exit(0));
    }
});
