module.exports = {
    clearMocks: true,
    collectCoverageFrom: ['src/**/*.{js,jsx,mjs}'],
    coverageDirectory: 'coverage',
    moduleFileExtensions: ['js', 'json', 'jsx'],
    setupFiles: ['<rootDir>/enzyme.config.js'],
    testEnvironment: 'jsdom',
    testMatch: ['**/__tests__/**/*.js?(x)', '**/?(*.)+(spec|test).js?(x)'],
    testURL: 'http://localhost',
    transformIgnorePatterns: ['<rootDir>/node_modules/'],
    verbose: false,
    snapshotSerializers: ['enzyme-to-json/serializer'],
    transform: {
        '^.+\\.(ts|tsx|js|jsx)$': '<rootDir>/node_modules/babel-jest',
    },
    moduleNameMapper: {
        '^.+\\.(css|less)$': '<rootDir>src/mock/CSSMock.js',
    },
};
