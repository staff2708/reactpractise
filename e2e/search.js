module.exports = {
    'Search case with founded films': function (browser) {
        browser
            .url('http://localhost:8080')
            .waitForElementVisible('body')
            .setValue('.search__input>.custom__input', 'Star Wars')
            .click('.search__button')
            .assert.elementPresent('.film-list__wrapper');
    },

    'Search case without founded films': function (browser) {
        browser
            .clearValue('.search__input>.custom__input')
            .setValue('.search__input>.custom__input', 'Film that you will never find')
            .click('.search__button')
            .waitForElementPresent('.no-films');
    },

    'Search case with founded films with changing find type': function (browser) {
        browser
            .clearValue('.search__input>.custom__input')
            .setValue('.search__input>.custom__input', 'Drama')
            .click('.search-filter__button--genre')
            .assert.cssClassPresent(
                '.search-filter__button--genre',
                'search-filter__button--active',
            )
            .assert.cssClassNotPresent(
                '.search-filter__button--title',
                'search-filter__button--active',
            )
            .click('.search__button')
            .assert.elementPresent('.film-list__wrapper');
    },

    'Search case without founded films with changing find type': function (browser) {
        browser
            .clearValue('.search__input>.custom__input')
            .setValue('.search__input>.custom__input', 'Star Wars')
            .click('.search-filter__button--genre')
            .assert.cssClassPresent(
                '.search-filter__button--genre',
                'search-filter__button--active',
            )
            .assert.cssClassNotPresent(
                '.search-filter__button--title',
                'search-filter__button--active',
            )
            .click('.search__button')
            .waitForElementPresent('.no-films')
            .end();
    },
};
