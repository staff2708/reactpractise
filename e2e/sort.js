module.exports = {
    'Sort films by release date case': function(browser) {
        browser
            .url('http://localhost:8080')
            .waitForElementVisible('body')
            .setValue('.search__input>.custom__input', 'Star Wars')
            .click('.search__button')
            .waitForElementVisible('.film-list__wrapper')
            .assert.containsText('.sort-filter__search-results', '9 films found');
        browser.expect.element('.film-preview__name').text.to.equal('Star Wars');
    },

    'Sort films by rating case': function(browser) {
        browser
            .click('.sort-filter__button--rating')
            .waitForElementVisible('.film-list__wrapper')
            .assert.containsText('.sort-filter__search-results', '9 films found');
        browser.expect.element('.film-preview__name').text.to.equal('Solo: A Star Wars Story');
        browser.end();
    },
};
